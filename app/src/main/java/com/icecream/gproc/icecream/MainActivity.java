package com.icecream.gproc.icecream;

import android.content.Intent;
import android.icu.text.NumberFormat;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int coffeePrice = 5;
    private int whippedCreamPrice = 2;
    private int chocolatePrice = 1;
    private int quantity = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // The method is called when the order button is clicked
    public void SubmitOrder(View view){
        SubmitOrder(quantity);
    }

    // Increment number of products
    public void Increment(View view){
        if (quantity < 99){
            quantity++;
            ShowOrders(quantity);
        }
    }

    // Decrement number of products
    public void Decrement(View view){
        if (quantity > 0){
            quantity--;
            ShowOrders(quantity);
        }
    }

    // Shows number of orders
    private void ShowOrders(int number){
        TextView quantityTextView = findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    // Show the price of the order
    private void SubmitOrder(int number){
        TextView priceTextView = findViewById(R.id.price_text_view);
        boolean isWhippedCreamChecked = ((CheckBox) findViewById(R.id.whipped_cream_checkbox)).isChecked();
        boolean isChocolateChecked = ((CheckBox) findViewById(R.id.chocolate_checkbox)).isChecked();
        int totalPrice = number * (coffeePrice + (isWhippedCreamChecked ? 1 : 0) * whippedCreamPrice + (isChocolateChecked ? 1 : 0) * chocolatePrice);

        // Strings can be referenced like this R.string.string_id, e.g.
        // String hello = getResourses().getString(R.string.app_name);
        // TextView textView = new TextView(this);
        // textView.setText(R.string.app_name);
        String returnMessage = "";
        returnMessage += "Name: " + ((EditText) findViewById(R.id.name_edit_text)).getText();
        returnMessage += "\nAdd whipped cream? " + isWhippedCreamChecked;
        returnMessage += "\nAdd chocolate? " + isChocolateChecked;
        returnMessage += "\nQuantity: " + number;
        returnMessage += "\nTotal: " + NumberFormat.getCurrencyInstance().format(totalPrice);
        returnMessage += "\nThank you!";

        priceTextView.setText(returnMessage);

        ComposeEmail(returnMessage);
    }

    // Compose an email
    private void ComposeEmail(String body){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Product Order Details From IceCream");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
